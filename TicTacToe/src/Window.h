/*
 * Window.h
 *
 *  Created on: Sep 27, 2020
 *      Author: oliver
 */

#ifndef TICTACTOE_SRC_WINDOW_H_
#define TICTACTOE_SRC_WINDOW_H_

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <memory>
#include <string>

namespace core {

	class Window {
	public:
	    Window() = default;
	    Window(int width, int height, const std::string& title);
	    bool ShouldClose();
	    void SwapBuffers();
	    ~Window();
	private:
	    GLFWwindow* m_handle;
	    int width, height;
	    std::string title;

	};

} /* namespace core */

#endif /* TICTACTOE_SRC_WINDOW_H_ */
