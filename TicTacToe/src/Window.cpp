/*
 * Window.cpp
 *
 *  Created on: Sep 27, 2020
 *      Author: oliver
 */

#include "Window.h"
#include <cassert>

namespace core {

	Window::Window(int width, int height, const std::string& title)
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		this->width = width;
		this->height = height;
		this->title = title;

		this->m_handle = glfwCreateWindow(this->width, this->height, this->title.c_str(), NULL, NULL);

		assert(this->m_handle);
	}
	Window::~Window()
	{
		delete m_handle;
	}

	bool Window::ShouldClose()
	{
		return glfwWindowShouldClose(this->m_handle);
	}
	void Window::SwapBuffers()
	{
		glfwSwapBuffers(this->m_handle);
		glfwPollEvents();
	}

} /* namespace core */
