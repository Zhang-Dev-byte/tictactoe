This Tic-Tac-Toe is made for learning purposes only!
Please do not use commercially.

LICENSE:
Tic-Tac-Toe game (c) by Oliver Zhang

Tic-Tac-Toe game is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.